package model.interfaces;

/**
 * 
 * Represents a luggage.
 */
public interface Luggage {

    /**
     * 
     * @return the luggage identifier
     */
    String getLuggageId();

    /**
     * 
     * @return the weight of the luggage
     */
    double getWeight();

}
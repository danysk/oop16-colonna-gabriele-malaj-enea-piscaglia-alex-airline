package controller.interfaces;

/**
 * Flight Conttroller interface.
 */
public interface FlightConttroller {

    /**
     * Removes a flight from the list of flights.
     */
    void removeFlight();

}
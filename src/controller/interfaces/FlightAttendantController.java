package controller.interfaces;

/**
 * Flight Attendant Controller interface.
 */
public interface FlightAttendantController {

    /**
     * Removes a flight attendant from the list of flight  attendants.
     */
    void removeFlightAttendant();

}
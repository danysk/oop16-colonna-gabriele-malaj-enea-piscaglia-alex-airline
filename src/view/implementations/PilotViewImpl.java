package view.implementations;

public class PilotViewImpl extends AddRemoveViewImpl {

	public PilotViewImpl() {
		super();
		this.setFrameTitle("Men� Piloti");
		this.setLabelTitle("Airline Pilots");
		this.setLabelViewedListTitle("Piloti disponibili:");
	}
}

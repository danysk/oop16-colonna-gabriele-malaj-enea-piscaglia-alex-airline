package view.implementations;

public class PlaneViewImpl extends AddRemoveViewImpl {

	public PlaneViewImpl() {
		super();
		this.setFrameTitle("Men� Aerei");
		this.setLabelTitle("Airline Airplanes");
		this.setLabelViewedListTitle("Aerei disponibili:");
	}
}

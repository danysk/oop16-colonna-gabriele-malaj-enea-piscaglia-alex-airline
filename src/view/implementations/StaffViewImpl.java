package view.implementations;

public class StaffViewImpl extends AddRemoveViewImpl {

	public StaffViewImpl() {
		super();
		this.setFrameTitle("Men� Staff");
		this.setLabelTitle("Airline Staff");
		this.setLabelViewedListTitle("Staff Attuale:");
	}
}
